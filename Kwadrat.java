import java.awt.Color;
import java.awt.Rectangle;
import java.util.Random;

import javax.swing.JPanel;

class Kwadrat {
	int x = rand.nextInt(500), y = rand.nextInt(500), dx = 1, dy = 1;
	int xKwadratu = 25;
	int yKwadratu = 25;
	int odbicieDol = 0, odbicieBok = 0;
	private Color clr;
	int sprawdz = 0, sprawdz2 = 0;
	int tick = 0;
	protected static final Random rand = new Random();

	public int getxKwadratu() {
		return xKwadratu;
	}

	public void setxKwadratu(int xKwadratu) {
		this.xKwadratu = xKwadratu;
	}

	public int getyKwadratu() {
		return yKwadratu;
	}

	public void setyKwadratu(int yKwadratu) {
		this.yKwadratu = yKwadratu;
	}

	public void ruszKwadrat(JPanel pojemnik) {
		Rectangle granicePanelu = pojemnik.getBounds();
		dx = 1 + rand.nextInt(30);
		dy = 1 + rand.nextInt(10);

		if (sprawdz == 1) {
			dy = -dy;
		}

		if (sprawdz2 == 1) {
			dx = -dx;
		}
		x += dx;
		y += dy;

		if (y + getyKwadratu() >= granicePanelu.getMaxY()) {
			y = (int) (granicePanelu.getMaxY() - getyKwadratu());
			sprawdz = 1;

		}
		if (x + getxKwadratu() >= granicePanelu.getMaxX()) {
			x = (int) (granicePanelu.getMaxX() - getxKwadratu());
			sprawdz2 = 1;
		}

		if (y < granicePanelu.getMinY()) {
			y = (int) granicePanelu.getMinY();
			sprawdz = 0;
		}

		if (x < granicePanelu.getMinX()) {
			x = (int) granicePanelu.getMinX();
			sprawdz2 = 0;
		}

	}

	public int zmienRozmiarX() {

		tick = rand.nextInt(2);
		int zmiana;

		if (tick % 2 == 0) {
			zmiana = getxKwadratu() + rand.nextInt(2);
			setxKwadratu(zmiana);

		} else {
			zmiana = getxKwadratu() - rand.nextInt(2);
			setxKwadratu(zmiana);
		}

		return getxKwadratu();
	}


	public Color zmienKolor() {
		clr = new Color(rand.nextInt(255), rand.nextInt(255), rand.nextInt(255), rand.nextInt(255));
		return clr;
	}
}