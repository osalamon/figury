import java.awt.Color;
import java.awt.Rectangle;
import java.util.Random;

import javax.swing.JPanel;

class Kolo{
	int x=rand.nextInt(500),y=rand.nextInt(500),dx =1,dy=1;
	int xKola=25;
	int yKola=25;
	int odbicieDol=0,odbicieBok=0;
	private Color clr;
	int sprawdz=0,sprawdz2=0;
	int tick=0;
	protected static final Random rand = new Random();
	
	public int getxKola() {
		return xKola;
	}

	public void setxKola(int xKola) {
		this.xKola = xKola;
	}

	public int getyKola() {
		return yKola;
	}

	public void setyKropelki(int yKola) {
		this.yKola = yKola;
	}

	
	
	

	
	public void ruszKolo(JPanel pojemnik) {
		Rectangle granicePanelu=pojemnik.getBounds();
		dx=1+rand.nextInt(30);
		dy=1+rand.nextInt(10);

		if(sprawdz==1) {
			dy=-dy;
		}
	
		if(sprawdz2==1) {
			dx=-dx;
		}
		x+=dx;
		y+=dy;

		if(y+getyKola() >=granicePanelu.getMaxY()) {
			y=(int)(granicePanelu.getMaxY()-getyKola());
			sprawdz=1;
			
		}
		if(x+getxKola() >=granicePanelu.getMaxX()) {
			x=(int)(granicePanelu.getMaxX()-getxKola());
			sprawdz2=1;
		}
		
		if(y<granicePanelu.getMinY()) {
			y=(int)granicePanelu.getMinY();
			sprawdz=0;
		}
		
		if(x<granicePanelu.getMinX()) {
			x=(int)granicePanelu.getMinX();
			sprawdz2=0;
		}
		

	}
	
	public int zmienRozmiar() {

		tick=rand.nextInt(2);
		int zmiana;
		
		if(tick%2==0) {
			zmiana=getxKola()+rand.nextInt(2);
			setxKola(zmiana);
			setyKropelki(zmiana);
			
		}else {
			zmiana=getxKola()-rand.nextInt(2);
			setxKola(zmiana);
			setyKropelki(zmiana);
		}
		
		return getxKola();
	}
	
	public Color zmienKolor() {
		clr = new Color(rand.nextInt(255), rand.nextInt(255), rand.nextInt(255), rand.nextInt(255));
		return clr;
	}
}