import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class Simple extends JFrame {
	private PanelAnimacji panelAnimacji = new PanelAnimacji();
	private JPanel panelButtonow = new JPanel();
	boolean kwadraty=true,kola=true,prostokaty=true;
	Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();



	public Simple() {
		this.setTitle("Rysowanie");
		this.setBounds(0, 0, screen.width, screen.height);

		JButton bKwadrat = (JButton) panelButtonow.add(new JButton("Kwadrat"));
		bKwadrat.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {

				startAnimation();
			}

		});

		JButton bKolo = (JButton) panelButtonow.add(new JButton("Koło"));
		bKolo.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {

				startAnimation2();
			}

		});

		JButton bProstokat = (JButton) panelButtonow.add(new JButton("Prostokąt"));
		bProstokat.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {

				startAnimation3();
			}

		});

		JButton bDelete = (JButton) panelButtonow.add(new JButton("Wyczysc panel"));
		bDelete.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {

				deleteAnimation();
			}

		});
		
		JButton bStop = (JButton) panelButtonow.add(new JButton("Zatrzymaj animacje"));
		bStop.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {

				stopAnimation();
			}

		});
		
		JButton bStart = (JButton) panelButtonow.add(new JButton("Wznow animacje"));
		bStart.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {

				resumeAnimation();
			}

		});

		this.getContentPane().add(panelAnimacji);
		this.getContentPane().add(panelButtonow, BorderLayout.SOUTH);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	public void startAnimation() {
		panelAnimacji.addKwadrat();
	}

	public void startAnimation2() {
		panelAnimacji.addKolo();
	}

	public void startAnimation3() {
		panelAnimacji.addProstokat();
	}

	public void deleteAnimation() {

		Object[] options = { "Kwadraty", "Koła", "Prostokąty" };
		int n = JOptionPane.showOptionDialog(null, "Które wątki usunąć?", "Wybór wątków",
				JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
	

		panelAnimacji.deleteAnimacji(n);

	}
	
	public void stopAnimation() {

		Object[] options = { "Kwadraty", "Koła", "Prostokąty" };
		int n = JOptionPane.showOptionDialog(null, "Które wątki zatrzymac?", "Wybór wątków",
				JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
	

		panelAnimacji.stopAnimacji(n);

	}
	
	public void resumeAnimation() {

		Object[] options = { "Kwadraty", "Koła", "Prostokąty" };
		int n = JOptionPane.showOptionDialog(null, "Które wątki wznowic?", "Wybór wątków",
				JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
	

		panelAnimacji.restartAnimacji(n);

	}
	

	public static void main(String[] args) {
		new Simple().setVisible(true);
	}

	class PanelAnimacji extends JPanel {

		ArrayList lista = new ArrayList();
		ArrayList lista2 = new ArrayList();
		ArrayList lista3 = new ArrayList();
		JPanel ten = this;
		Thread watek, watek2, watek3;
		ThreadGroup grupaWatkow = new ThreadGroup("Grupa kwadratow");
		ThreadGroup grupaWatkow2 = new ThreadGroup("Grupa kol");
		ThreadGroup grupaWatkow3 = new ThreadGroup("Grupa prostokatow");

		public void addKwadrat() {
			lista.add(new Kwadrat());
			watek = new Thread(grupaWatkow, new KwadratRunnable((Kwadrat) lista.get(lista.size() - 1)));
			watek.start();
			grupaWatkow.list();
		}

		public void addKolo() {
			lista2.add(new Kolo());
			watek2 = new Thread(grupaWatkow2, new KoloRunnable((Kolo) lista2.get(lista2.size() - 1)));
			watek2.start();
			grupaWatkow2.list();
		}

		public void addProstokat() {
			lista3.add(new Prostokat());
			watek3 = new Thread(grupaWatkow3, new ProstokatRunnable((Prostokat) lista3.get(lista3.size() - 1)));
			watek3.start();
			grupaWatkow3.list();
		}

		public void deleteAnimacji(int n) {
			
			if(n==0) {
				grupaWatkow.interrupt();
			}else if(n==1) {
				grupaWatkow2.interrupt();
			}else if(n==2) {
				grupaWatkow3.interrupt();
			}
			
		
		}
		public void stopAnimacji(int n) {
			
			if(n==0) {
				grupaWatkow.suspend();
				kwadraty=false;
			}else if(n==1) {
				grupaWatkow2.suspend();
				kola=false;
			}else if(n==2) {
				grupaWatkow3.suspend();
				prostokaty=false;
			}
			
		
		}
		public void restartAnimacji(int n) {
			
			if(n==0) {
				grupaWatkow.resume();
				kwadraty=true;
			}else if(n==1) {
				grupaWatkow2.resume();
				kola=true;
			}else if(n==2) {
				grupaWatkow3.resume();
				prostokaty=true;
			}
			
		
		}

		@Override
		public void paintComponent(Graphics g) {
			super.paintComponent(g);
			int rozmiarx, rozmiary;
			Graphics2D g2d = (Graphics2D) g;
			
			for (int i = 0; i < lista.size(); i++) {
				rozmiarx = ((Kwadrat) lista.get(i)).zmienRozmiarX();
				g2d.setColor(((Kwadrat) lista.get(i)).zmienKolor());
				g2d.fillRect(((Kwadrat) lista.get(i)).x, ((Kwadrat) lista.get(i)).y, rozmiarx, rozmiarx);
			}
			
			
			for (int i = 0; i < lista2.size(); i++) {
				rozmiarx = ((Kolo) lista2.get(i)).zmienRozmiar();
				g2d.setColor(((Kolo) lista2.get(i)).zmienKolor());
				g2d.fillOval(((Kolo) lista2.get(i)).x, ((Kolo) lista2.get(i)).y, rozmiarx, rozmiarx);

			}
			
			
			for (int i = 0; i < lista3.size(); i++) {
				rozmiarx = ((Prostokat) lista3.get(i)).zmienRozmiarX();
				rozmiary = ((Prostokat) lista3.get(i)).zmienRozmiarY();
				g2d.setColor(((Prostokat) lista3.get(i)).zmienKolor());
				g2d.fillRect(((Prostokat) lista3.get(i)).x, ((Prostokat) lista3.get(i)).y, rozmiarx, rozmiary);
			}
			

			g.dispose();
		}

		public class KwadratRunnable implements Runnable {

			Kwadrat kwadrat;

			public KwadratRunnable(Kwadrat kwadrat) {
				this.kwadrat = kwadrat;
			}

			@Override
			public void run() {

				try {
					while (!Thread.currentThread().isInterrupted()) {
						this.kwadrat.ruszKwadrat(ten);
						repaint();
						Thread.sleep(30);
					}
				} catch (InterruptedException ex) {
					// TODO Auto-generated catch block
					System.out.println(ex.getMessage());
					lista.clear();
					repaint();
				}

			}

		}

		public class KoloRunnable implements Runnable {
			Kolo kolo;

			public KoloRunnable(Kolo kolo) {
				this.kolo = kolo;
			}

			@Override
			public void run() {
				try {
					while (!Thread.currentThread().isInterrupted()) {
						this.kolo.ruszKolo(ten);
						repaint();
						Thread.sleep(40);
					}
				} catch (InterruptedException ex) {
					// TODO Auto-generated catch block
					System.out.println(ex.getMessage());
					lista2.clear();
					repaint();
				}

			}

		}

		public class ProstokatRunnable implements Runnable {
			Prostokat prostokat;

			public ProstokatRunnable(Prostokat prostokat) {
				this.prostokat = prostokat;
			}

			@Override
			public void run() {
				try {
					while (!Thread.currentThread().isInterrupted()) {
						this.prostokat.ruszProstokat(ten);
						repaint();
						Thread.sleep(50);
					}
				} catch (InterruptedException ex) {
					// TODO Auto-generated catch block
					System.out.println(ex.getMessage());
					lista3.clear();
					repaint();
				}

			}

		}

	}

}
