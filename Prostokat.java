import java.awt.Color;
import java.awt.Rectangle;
import java.util.Random;

import javax.swing.JPanel;

class Prostokat {
	int x = rand.nextInt(500), y = rand.nextInt(500), dx = 1, dy = 1;
	int xProstkata = 25;
	int yProstokata = 25;
	int odbicieDol = 0, odbicieBok = 0;
	private Color clr;
	int sprawdz = 0, sprawdz2 = 0;
	int tick = 0;
	protected static final Random rand = new Random();

	public int getxProstkata() {
		return xProstkata;
	}

	public void setxProstkata(int xProstkata) {
		this.xProstkata = xProstkata;
	}

	public int getyProstokata() {
		return yProstokata;
	}

	public void setyKwadratu(int yProstokata) {
		this.yProstokata = yProstokata;
	}

	public void ruszProstokat(JPanel pojemnik) {
		Rectangle granicePanelu = pojemnik.getBounds();
		dx = 1 + rand.nextInt(30);
		dy = 1 + rand.nextInt(10);

		if (sprawdz == 1) {
			dy = -dy;
		}

		if (sprawdz2 == 1) {
			dx = -dx;
		}
		x += dx;
		y += dy;

		if (y + getyProstokata() >= granicePanelu.getMaxY()) {
			y = (int) (granicePanelu.getMaxY() - getyProstokata());
			sprawdz = 1;

		}
		if (x + getxProstkata() >= granicePanelu.getMaxX()) {
			x = (int) (granicePanelu.getMaxX() - getxProstkata());
			sprawdz2 = 1;
		}

		if (y < granicePanelu.getMinY()) {
			y = (int) granicePanelu.getMinY();
			sprawdz = 0;
		}

		if (x < granicePanelu.getMinX()) {
			x = (int) granicePanelu.getMinX();
			sprawdz2 = 0;
		}

	}

	public int zmienRozmiarX() {

		tick = rand.nextInt(2);
		int zmiana;

		if (tick % 2 == 0) {
			zmiana = getxProstkata() + rand.nextInt(2);
			setxProstkata(zmiana);

		} else {
			zmiana = getxProstkata() - rand.nextInt(2);
			setxProstkata(zmiana);
		}

		return getxProstkata();
	}

	public int zmienRozmiarY() {

		tick = rand.nextInt(2);
		int zmiana;

		if (tick % 2 == 0) {
			zmiana = getyProstokata() + rand.nextInt(2);
			setyKwadratu(zmiana);

		} else {
			zmiana = getyProstokata() - rand.nextInt(2);
			setyKwadratu(zmiana);
		}

		return getyProstokata();
	}

	public Color zmienKolor() {
		clr = new Color(rand.nextInt(255), rand.nextInt(255), rand.nextInt(255), rand.nextInt(255));
		return clr;
	}
}